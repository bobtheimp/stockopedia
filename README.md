# README #

Technical test app for Stockopedia

### What is this repository for? ###

* PHP7.4 / Symfony 5.3 Controller API example

### How do I get set up? ###

* Clone the repository
*  Ensure docker-compose is installed and working
*  Add docker.con to your hosts file set to 172.17.0.1 (or whatever docker is using as the bridge IP on your
   installation)
*  Create a Files directory at the base level and copy the test files into it (this is for the initial DB migration)
*  Execute "docker-compose build && docker-compose up -d" in the docker directory
  * Execute the Doctrine migrations in the container to bring up the DB schema
* Send an API request to docker.con:8000/api/process and enjoy the results

### Notes and improvements ###

* Not 100% happy with the OperatorBase.php class, but it all seems to work.
* Test cases are not quite complete, the OperatorBaseTest class looks for Throwable instead of an actual class,
  this was down to time constraints, I would have included the expected exception in the test data  provider.
  Also, the data is using the "live" data, again, time constraints - otherwise I would either use Mockery or a test DB.
* The Multiplication class has been implemented, it would be easy to add more, simply 
  add the relevant operator and class to the OperatorFactory class to the 
  $supportedOperators array. Then add the class itself to the 
  App/Service/DSL/Operator namespace. The validateData method in the OperatorBase class will need overriding 
  to ensure all data is correctly screened, especially for the subtraction operator as the 'a' and 'b' attributes are 
  arrays in that DSL.