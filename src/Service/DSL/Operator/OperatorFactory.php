<?php
declare(strict_types=1);
/**
 * Description:
 *     Determine the operator class to use
 */

namespace App\Service\DSL\Operator;

use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OperatorFactory
 * @package App\Service\DSL\Operator
 */
class OperatorFactory
{
    private array $supportedOperators = [
        '*' => OperatorMultiply::class
    ];

    /** @var ContainerInterface $container */
    private ContainerInterface $container;

    /**
     * OperatorFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $operator
     * @return object
     */
    public function create(string $operator): object
    {
        if (empty($operator)) {
            throw new InvalidArgumentException("Empty or missing 'fn' expression attribute");
        }

        if (!array_key_exists($operator, $this->supportedOperators)) {
            throw new InvalidArgumentException("Operator '$operator' is unsupported");
        }

        if (!class_exists($this->supportedOperators[$operator])) {
            throw new InvalidArgumentException("Class for '$operator' operator is not found");
        }

        return $this->container->get($this->supportedOperators[$operator]);
    }
}