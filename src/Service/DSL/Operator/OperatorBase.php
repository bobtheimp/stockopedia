<?php
declare(strict_types=1);
/**
 * Description:
 *     base class for all common code shared by operator classes
 */

namespace App\Service\DSL\Operator;

use App\Entity\Attributes;
use App\Entity\Facts;
use App\Entity\Securities;
use App\EntityHandler\AttributesEntityHandler;
use App\EntityHandler\FactsEntityHandler;
use App\EntityHandler\SecuritiesEntityHandler;
use InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OperatorBase
 * @package App\Service\DSL\Operator
 */
class OperatorBase
{
    /** @var Securities|null $securities */
    protected ?Securities $securities;

    /** @var Attributes|null $attributes */
    protected ?Attributes $attributes;

    /** @var Facts|null $facts */
    protected ?Facts $facts;

    /** @var SecuritiesEntityHandler $securitiesEntityHandler */
    protected SecuritiesEntityHandler $securitiesEntityHandler;

    /** @var AttributesEntityHandler $attributesEntityHandler */
    protected AttributesEntityHandler $attributesEntityHandler;

    /** @var FactsEntityHandler $factsEntityHandler */
    protected FactsEntityHandler $factsEntityHandler;

    /**
     * OperatorBase constructor.
     * @param SecuritiesEntityHandler $securitiesEntityHandler
     * @param AttributesEntityHandler $attributesEntityHandler
     * @param FactsEntityHandler $factsEntityHandler
     */
    public function __construct(SecuritiesEntityHandler $securitiesEntityHandler,
                                AttributesEntityHandler $attributesEntityHandler,
                                FactsEntityHandler $factsEntityHandler)
    {
        $this->securitiesEntityHandler = $securitiesEntityHandler;
        $this->attributesEntityHandler = $attributesEntityHandler;
        $this->factsEntityHandler = $factsEntityHandler;
    }

    /**
     * @param array $expression
     * @param string $security
     * @return $this
     */
    public function validateData(array $expression, string $security): self
    {
        $this->securities = $this->getSecuritiesEntity($security);
        if (! $this->securities instanceof Securities) {
            throw new NotFoundHttpException("Could not find securities entry for symbol '$security'");
        }

        $attributeName = $expression['a'] ?? '';
        $this->attributes = $this->getAttributesEntity($attributeName);
        if (! $this->attributes instanceof Attributes) {
            throw new NotFoundHttpException("Unable to find attribute for '$attributeName'");
        }

        $this->facts = $this->getFactsEntity();
        if (! $this->facts instanceof Facts) {
            throw new NotFoundHttpException(
                "Unable to find facts linked to attribute '{$this->attributes->getId()}' and security '{$this->securities->getId()}'"
            );
        }

        return $this;
    }

    /**
     * @param string $name
     * @return Securities|null
     */
    protected function getSecuritiesEntity(string $name): ?Securities
    {
        if (empty($name)) {
            throw new InvalidArgumentException('Empty securities element');
        }
        return $this->securitiesEntityHandler->getSecuritiesBySymbol($name);
    }

    /**
     * @param string $name
     * @return Attributes|null
     */
    protected function getAttributesEntity(string $name): ?Attributes
    {
        if (empty($name)) {
            throw new InvalidArgumentException("Empty or missing expression attribute 'a'");
        }
        return $this->attributesEntityHandler->getAttributesByName($name);
    }

    /**
     * @return Facts|null
     */
    protected function getFactsEntity(): ?Facts
    {
        return $this->factsEntityHandler->getFactByParams([
            'securityId' => $this->securities->getId(),
            'attributeId' => $this->attributes->getId()
        ]);
    }
}