<?php
declare(strict_types=1);
/**
 * Define the "abstract" method to use for the operation classes
 */

namespace App\Service\DSL\Operator;

/**
 * Interface OperatorInterface
 * @package App\Service\DSL\Operator
 */
interface OperatorInterface
{
    /**
     * @return array
     */
    public function execute(): array;

    /**
     * @param array $expression
     * @param string $security
     * @return OperatorInterface
     */
    public function validateData(array $expression, string $security): self;
}
