<?php
declare(strict_types=1);
/**
 * Description:
 *     Addition operator
 */

namespace App\Service\DSL\Operator;

use InvalidArgumentException;

/**
 * Class OperatorMultiply
 * @package App\Service\DSL\Operator
 */
class OperatorMultiply extends OperatorBase implements OperatorInterface
{
    /** @var float|null $multiplier */
    private ?float $multiplier;

    /**
     * @return array
     */
    public function execute(): array
    {
        return [number_format(($this->facts->getValue() * $this->multiplier), 2)];
    }

    /**
     * @param array $expression
     * @param string $security
     * @return OperatorMultiply
     */
    public function validateData(array $expression, string $security): self
    {
        parent::validateData($expression, $security);

        $this->multiplier = $expression['b'] ?? null;
        if (null === $this->multiplier) {
            throw new InvalidArgumentException("Empty or missing expression attribute 'b'");
        }

        return $this;
    }
}
