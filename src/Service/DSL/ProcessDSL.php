<?php
declare(strict_types=1);
/**
 * Description:
 *     Process the incoming DSL statements
 */

namespace App\Service\DSL;

use App\Service\DSL\Operator\OperatorFactory;
use Throwable;

/**
 * Class ProcessDSL
 * @package App\Service\DSL
 */
class ProcessDSL
{
    /** @var OperatorFactory $operatorFactory */
    private OperatorFactory $operatorFactory;

    /**
     * ProcessDSL constructor.
     * @param OperatorFactory $operatorFactory
     */
    public function __construct(OperatorFactory $operatorFactory)
    {
        $this->operatorFactory = $operatorFactory;
    }

    /**
     * @param array $jsonArray
     * @return array
     */
    public function processDSL(array $jsonArray): array
    {
        if (empty($jsonArray)) {
            return ['Empty request'];
        }

        $expression = $jsonArray['expression'] ?? [];
        $security = $jsonArray['security'] ?? '';

        try {
            $operator = $expression['fn'] ?? '';
            $result = ['result' => $this->operatorFactory
                ->create($operator)
                ->validateData($expression, $security)
                ->execute()];
        } catch (Throwable $exception) {
            $result = ['error' => $exception->getMessage()];
        }

        return $result;
    }
}