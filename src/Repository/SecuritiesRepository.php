<?php
declare(strict_types=1);
/**
 * Description:
 *
 * @package App\Repository
 */

namespace App\Repository;

use App\Entity\Securities;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class SecuritiesRepository
 *
 * @package App\Repository
 *
 * @method Securities|null find($id, $lockMode = null, $lockVersion = null)
 * @method Securities|null findOneBy(array $criteria, array $orderBy = null)
 * @method Securities[]    findAll()
 * @method Securities[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null) *
 */
class SecuritiesRepository extends ServiceEntityRepository
{
    /**
     * SecuritiesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Securities::class);
    }
}
