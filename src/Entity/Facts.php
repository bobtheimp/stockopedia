<?php
declare(strict_types=1);
/**
 * Description:
 * Facts entity
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Facts
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\FactsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="facts")
 */
class Facts
{
    /**
     * @ORM\ManyToOne(targetEntity="Securities")
     * @ORM\JoinColumn(name="security_id", referencedColumnName="id")
     * @ORM\Id @ORM\Column(type="integer", name="security_id")
     */
    private int $securityId;

    /**
     * @ORM\ManyToOne(targetEntity="Attributes")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * @ORM\Id @ORM\Column(type="integer", name="attribute_id")
     */
    private int $attributeId;

    /**
     * @ORM\Id @ORM\Column(type="decimal", precision=8, scale=2, name="value")
     */
    private float $value;

    /**
     * @return int|null
     */
    public function getAttributeId(): ?int
    {
        return $this->attributeId;
    }

    /**
     * @param int $attributeId
     * @return $this
     */
    public function setAttributeId(int $attributeId): self
    {
        $this->attributeId = $attributeId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSecurityId(): ?int
    {
        return $this->securityId;
    }

    /**
     * @param int $securityId
     * @return $this
     */
    public function setSecurityId(int $securityId): self
    {
        $this->securityId = $securityId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }
}