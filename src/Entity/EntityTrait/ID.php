<?php
declare(strict_types=1);
/**
 * Description:
 * hold the "id" field property as it is identical in every entity
 *
 * @package App\Entity\EntityTrait
 */

namespace App\Entity\EntityTrait;

/**
 * Trait ID
 *
 * @package App\Entity\EntityTrait
 */
trait ID
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}