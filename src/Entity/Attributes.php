<?php
declare(strict_types=1);
/**
 * Description:
 * Attributes entity
 */

namespace App\Entity;

use App\Entity\EntityTrait\ID;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Attributes
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\AttributesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="attributes")
 */
class Attributes
{
    /**
     * Add the property id along with its getter
     */
    use ID;

    /**
     * @ORM\Column(type="string", length=25, unique=true, name="name")
     */
    private string $name;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

}
