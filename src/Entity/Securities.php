<?php
declare(strict_types=1);
/**
 * Description:
 * Securities entity
 */

namespace App\Entity;

use App\Entity\EntityTrait\ID;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Securities
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\SecuritiesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="securities")
 */
class Securities
{
    /**
     * Add the property id along with its getter
     */
    use ID;

    /**
     * @ORM\Column(type="string", length=5, unique=true, name="symbol")
     */
    private string $symbol;

    /**
     * @return string|null
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @param $symbol
     * @return $this
     */
    public function setSymbol($symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

}
