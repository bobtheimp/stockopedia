<?php
declare(strict_types=1);
/**
 * Description:
 *     Entity Handler for the Securities entity
 */

namespace App\EntityHandler;

use App\Entity\Securities;
use App\Repository\SecuritiesRepository;

/**
 * Class SecuritiesEntityHandler
 * @package App\EntityHandler
 */
class SecuritiesEntityHandler
{
    /** @var SecuritiesRepository $securitiesRepository */
    private SecuritiesRepository $securitiesRepository;

    /**
     * SecuritiesEntityHandler constructor.
     * @param SecuritiesRepository $securitiesRepository
     */
    public function __construct(SecuritiesRepository $securitiesRepository)
    {
        $this->securitiesRepository = $securitiesRepository;
    }

    /**
     * @param string $symbol
     * @return Securities|null
     */
    public function getSecuritiesBySymbol(string $symbol): ?Securities
    {
        return $this->securitiesRepository->findOneBy(['symbol' => $symbol]);
    }
}
