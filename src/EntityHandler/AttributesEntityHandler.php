<?php
declare(strict_types=1);
/**
 * Description:
 *     Entity Handler for the Attributes entity
 */

namespace App\EntityHandler;

use App\Entity\Attributes;
use App\Repository\AttributesRepository;

class AttributesEntityHandler
{
    /** @var AttributesRepository $attributesRepository */
    private AttributesRepository $attributesRepository;

    /**
     * SecuritiesEntityHandler constructor.
     * @param AttributesRepository $attributesRepository
     */
    public function __construct(AttributesRepository $attributesRepository)
    {
        $this->attributesRepository = $attributesRepository;
    }

    /**
     * @param string $name
     * @return Attributes|null
     */
    public function getAttributesByName(string $name): ?Attributes
    {
        return $this->attributesRepository->findOneBy(['name' => $name]);
    }
}