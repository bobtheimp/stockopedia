<?php
declare(strict_types=1);
/**
 * Description:
 *     Entity Handler for the Facts entity
 */

namespace App\EntityHandler;

use App\Entity\Facts;
use App\Repository\AttributesRepository;
use App\Repository\FactsRepository;

class FactsEntityHandler
{
    /** @var FactsRepository $factsRepository */
    private FactsRepository $factsRepository;

    /**
     * SecuritiesEntityHandler constructor.
     * @param FactsRepository $factsRepository
     */
    public function __construct(FactsRepository $factsRepository)
    {
        $this->factsRepository = $factsRepository;
    }

    /**
     * @param array $params
     * @return Facts|null
     */
    public function getFactByParams(array $params): ?Facts
    {
        return $this->factsRepository->findOneBy($params);
    }
}