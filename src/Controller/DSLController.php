<?php
declare(strict_types=1);
/**
 * Description:
 *     Controller for incoming REST API calls
 */

namespace App\Controller;

use App\Service\DSL\ProcessDSL;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DSLController
 * @package App\Controller
 */
class DSLController extends AbstractController
{
    /** @var ProcessDSL $processDSL */
    private ProcessDSL $processDSL;

    /**
     * DSLController constructor.
     * @param ProcessDSL $processDSL
     */
    public function __construct(ProcessDSL $processDSL)
    {
        $this->processDSL = $processDSL;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route ("/api/process")
     */
    public function process(Request $request): Response
    {
        return $this->json($this->processDSL->processDSL($request->toArray()));
    }
}