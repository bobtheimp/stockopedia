<?php
declare(strict_types=1);
/**
 * Test case for DSL Multiply Operator class
 */

namespace App\Tests\App\tests\Service;

use App\Service\DSL\Operator\OperatorFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class OperatorMultiplyTest
 * @package App\Tests\App\tests\Service
 */
class OperatorMultiplyTest extends WebTestCase
{
    /**
     * @param array $testParameters
     * @dataProvider operatorMultiplyProvider
     */
    public function testMultiplyOperator(array $testParameters): void
    {
        $operatorFactory = self::getContainer()->get(OperatorFactory::class);

        $expression = $testParameters['expression'];
        $security = $testParameters['security'];
        $expected = $testParameters['expected'];

        $result = $operatorFactory
            ->create($expression['fn'])
            ->validateData($expression, $security)
            ->execute();

        self::assertEquals($expected, $result);

        unset($operatorFactory);
    }

    /**
     * @return array
     */
    public function operatorMultiplyProvider(): array
    {
        return [
            ['multiplyABC' =>
                    [
                        'expression' => ['fn' => '*', 'a' => 'sales', 'b' => 2],
                        'security' => 'ABC',
                        'expected' => ['8.00']
                    ]
            ],
            ['multiplyBCD' =>
                [
                    'expression' => ['fn' => '*', 'a' => 'sales', 'b' => 4],
                    'security' => 'BCD',
                    'expected' => ['32.00']
                ],
            ]
        ];
    }

    // TODO: haven't generated tests for missing attribute B, but this is covered by other tests
}
