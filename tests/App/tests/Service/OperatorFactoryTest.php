<?php
declare(strict_types=1);
/**
 * Test case for DSL OperatorFactory class
 */

namespace App\Tests\App\tests\Service;

use App\Service\DSL\Operator\OperatorFactory;
use App\Service\DSL\Operator\OperatorMultiply;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use InvalidArgumentException;

/**
 * Class OperatorFactoryTest
 * @package App\Tests\App\tests\Service
 */
class OperatorFactoryTest extends WebTestCase
{
    /**
     * @param string $operator
     * @param string $expected
     * @dataProvider operatorTestProvider
     */
    public function testCreateOperator(string $operator, string $expected): void
    {
        $operatorFactory = self::getContainer()->get(OperatorFactory::class);

        echo "Testing factory class object creation for '$operator' operator".PHP_EOL;

        self::assertInstanceOf($expected, $operatorFactory->create($operator));

        unset($operatorFactory);
    }

    /**
     * @param string $operator
     * @dataProvider operatorTestProviderBad
     */
    public function testFailCreateoperator(string $operator): void
    {
        $operatorFactory = self::getContainer()->get(OperatorFactory::class);

        echo "Testing factory class object creation failure for '$operator' operator".PHP_EOL;

        self::expectException(InvalidArgumentException::class);
        $operatorFactory->create($operator);

        unset($operatorFactory);
    }

    /**
     * @return array
     */
    public function operatorTestProvider(): array
    {
        return [
            ['*', OperatorMultiply::class],
        ];
    }

    /**
     * @return array
     */
    public function operatorTestProviderBad(): array
    {
        return [['+'],['']];
    }
}
