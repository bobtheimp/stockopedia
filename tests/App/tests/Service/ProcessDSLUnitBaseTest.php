<?php
declare(strict_types=1);
/**
 * Unit test case for DSL Process class
 */

namespace App\Tests\App\tests\Service;

use App\Service\DSL\ProcessDSL;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ProcessDSLTest
 * @package App\Tests\App\tests\Service
 */
class ProcessDSLUnitBaseTest extends WebTestCase
{
    /**
     * @param array $jsonArray
     * @dataProvider jsonProvider
     */
    public function testProcessDSL(array $jsonArray): void
    {
        $processDSL =  self::getContainer()->get(ProcessDSL::class);

        $result = $processDSL->processDSL(json_decode($jsonArray['payload'], true));
        $this->assertEquals($jsonArray['expected'], $result);

        unset($processDSL);
    }

    /**
     * @return array[]
     */
    public function jsonProvider(): array
    {
        return [
            [
                'Empty request' => [
                    'payload' => '{}',
                    'expected' => ["Empty request"]
                ]
            ],
            [
                'MultiplySuccess' => [
                    'payload' => '{"expression":{"fn":"*","a":"sales","b":2},"security":"ABC"}',
                    'expected' => ['result' => ['8.00']]
                ]
            ],
            [
                'UnknownFunction' => [
                    'payload' => '{"expression":{"fn":"+","a":"sales","b":2},"security":"ABC"}',
                    'expected' => ['error' => "Operator '+' is unsupported"]
                ]
            ],
            [
                'MissingAattribute' => [
                    'payload' => '{"expression":{"fn":"*","":"sales","b":2},"security":"ABC"}',
                    'expected' => ['error' => "Empty or missing expression attribute 'a'"]
                ]
            ],
            [
                'MissingBattribute' => [
                    'payload' => '{"expression":{"fn":"*","a":"sales"},"security":"ABC"}',
                    'expected' => ['error' => "Empty or missing expression attribute 'b'"]
                ]
            ],
            [
                'MissingSecurityattribute' => [
                    'payload' => '{"expression":{"fn":"*","a":"sales","b":2},"security":""}',
                    'expected' => ['error' => 'Empty securities element']
                ]
            ],
            [
                'SecurityNotFound' => [
                    'payload' => '{"expression":{"fn":"*","a":"sales","b":2},"security":"NOPE"}',
                    'expected' => ['error' => "Could not find securities entry for symbol 'NOPE'"]
                ]
            ],
            [
                'AttributeNotFound' => [
                    'payload' => '{"expression":{"fn":"*","a":"bob","b":2},"security":"ABC"}',
                    'expected' => ['error' => "Unable to find attribute for 'bob'"]
                ]
            ]
        ];
    }
}
