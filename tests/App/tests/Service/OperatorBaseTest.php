<?php
declare(strict_types=1);
/**
 * Test case for DSL Operator Base class
 */

namespace App\Tests\App\tests\Service;

use App\Service\DSL\Operator\OperatorFactory;
use App\Service\DSL\Operator\OperatorMultiply;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Throwable;

/**
 * Class OperatorBaseTest
 * @package App\Tests\App\tests\Service
 */
class OperatorBaseTest extends WebTestCase
{
    /**
     * @param array $testParameters
     * @dataProvider operatorBaseProvider
     */
    public function testOperatorBase(array $testParameters): void
    {
        $operatorFactory = self::getContainer()->get(OperatorFactory::class);

        $expression = $testParameters['expression'];
        $security = $testParameters['security'];
        $expected = $testParameters['expected'];

        $result = $operatorFactory
            ->create($expression['fn'])
            ->validateData($expression, $security);

        self::assertInstanceOf($expected, $result);

        unset($operatorFactory);
    }

    /**
     * @param array $testParameters
     * @dataProvider operatorBaseProviderBad
     */
    public function testOperatorBaseBad(array $testParameters): void
    {
        $operatorFactory = self::getContainer()->get(OperatorFactory::class);

        $expression = $testParameters['expression'];
        $security = $testParameters['security'];

        $this->expectException(Throwable::class);
        $operatorFactory
            ->create($expression['fn'])
            ->validateData($expression, $security);

        unset($operatorFactory);
    }

    /**
     * @return array
     */
    public function operatorBaseProvider(): array
    {
        return [
            ['multiply' =>
                [
                    'expression' => ['fn' => '*', 'a' => 'sales', 'b' => 2],
                    'security' => 'ABC',
                    'expected' => OperatorMultiply::class
            ]
        ]];
    }

    /**
     * @return array
     */
    public function operatorBaseProviderBad(): array
    {
        return [
            ['UnsupportedFunction' =>
                [
                    'expression' => ['fn' => '+', 'a' => 'sales', 'b' => 2],
                    'security' => 'ABC',
                ]
            ],
            ['MissingAttributeA' =>
                [
                    'expression' => ['fn' => '+', 'b' => 2],
                    'security' => 'ABC',
                ]
            ],
            ['MissingAttributeB' =>
                [
                    'expression' => ['fn' => '+', 'a' => 'sales'],
                    'security' => 'ABC',
                ]
            ],
            ['UnknownAttributeA' =>
                [
                    'expression' => ['fn' => '+', 'a' => 'nope', 'b' => 2],
                    'security' => 'ABC',
                ]
            ],
            ['UnknownSecurity' =>
                [
                    'expression' => ['fn' => '+', 'a' => 'sales', 'b' => 2],
                    'security' => 'NOPE',
                ]
            ],
            ['MissingSecurity' =>
                [
                    'expression' => ['fn' => '+', 'a' => 'sales', 'b' => 2],
                    'security' => '',
                ]
            ]
        ];
    }
}
