<?php
declare(strict_types=1);
/**
 * Test case for DSL Controller api URL
 */

namespace App\Tests\App\tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CalculatorInterfaceTest
 * @package App\tests
 */
final class DSLControllerTest extends WebTestCase
{
    /**
     * test the URL is retrieved OK
     */
    public function testDSLControllerEntryPoint(): void
    {
        $client = self::createClient();
        $client->request('GET',
            '/api/process',
            [],
            [],
            [],
            json_encode([
                'headers' => ['Content-Type' => 'application/json'],
                'json' => []
            ])
        );
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'REQUEST IS : '.$client->getRequest()
        );

        unset($client);
    }

    // TODO: add tests for content
}