<?php
declare(strict_types=1);
/**
 * Initial migration to create schema
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210614204843
 * @package DoctrineMigrations
 */
final class Version20210614204843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creating initial schema.';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE attributes (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, UNIQUE INDEX UNIQ_319B9E705E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facts (security_id INT NOT NULL, attribute_id INT NOT NULL, value NUMERIC(8, 2) NOT NULL, PRIMARY KEY(security_id, attribute_id, value)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE securities (id INT AUTO_INCREMENT NOT NULL, symbol VARCHAR(5) NOT NULL, UNIQUE INDEX UNIQ_A8210B24ECC836F9 (symbol), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->populateTablesFromFiles();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE attributes');
        $this->addSql('DROP TABLE facts');
        $this->addSql('DROP TABLE securities');
    }

    /*
     * a quick and dirty routine to populate the data tables from the supplied CSV files
     * note: I had to modify the field names from attributeId and securityId to attribute_id and security_id
     * in order for the headers to be picked up correctly as the file included the underscore in the headers
     */
    private function populateTablesFromFiles(): void
    {
        echo "Checking for files...".PHP_EOL;

        $basedir = 'Files';
        $files = glob("{$basedir}/*.csv");

        echo 'Found '.count($files).' files to process.'.PHP_EOL;

        foreach ($files as $filename) {
            $fullPath = "{$basedir}/{$filename}";
            $tableName = substr(pathinfo($fullPath, PATHINFO_FILENAME), 2);
            $handle = fopen($filename, 'rb');
            if (!is_resource($handle)) {
                exit("Unable to open file '{$filename}' for reading");
            }

            echo "Processing {$filename}...".PHP_EOL;

            $headers = [];
            while (!feof($handle)) {
                $line = fgetcsv($handle, 4096);
                if (empty($line)) {
                    continue;
                }

                if (!is_numeric($line[0])) {
                    $headers=$line;
                    continue;
                }

                $sql = "INSERT INTO {$tableName} (";
                foreach ($headers as $fieldName) {
                    $sql .= "{$fieldName},";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ") VALUES (";
                foreach ($line as $value) {
                    $sql .= "'{$value}',";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ')';

                echo $sql.PHP_EOL;
                $this->addSql($sql);
            }
        }
    }
}
